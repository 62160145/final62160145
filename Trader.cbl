       IDENTIFICATION DIVISION.
       PROGRAM-ID. TRADER5.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT TRADER5-FILE ASSIGN TO "trader5.dat"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  TRADER5-FILE.
       01  PROVINCE.
           88 END-OF-TRADER-FILE VALUE HIGH-VALUES.
           05 PROVINCE-ID PIC 9(2).
           05 TRADER-ID  PIC 9(4).
           05 MEMBER-INCOM PIC 9(6).
       WORKING-STORAGE SECTION. 
       01 P-INCOME.
           05 P-INCOM-TOTAL PIC 9(8)V99 OCCURS 50 TIMES.

       01 TOTAL-ID PIC 99.
       01 MEMBER PIC 99.
       01 MEMBER-TOTAL-INCOM PIC $$$,$$$,$$9.99.

       PROCEDURE DIVISION.
       BEGIN.
           MOVE ZERO TO  P-INCOME.
           OPEN INPUT TRADER5-FILE
           READ TRADER5-FILE
             AT END SET END-OF-TRADER-FILE TO TRUE
           END-READ
           PERFORM UNTIL END-OF-TRADER-FILE
             ADD MEMBER-INCOM TO P-INCOM-TOTAL(TRADER-ID)
           READ TRADER5-FILE
             AT END SET END-OF-TRADER-FILE TO TRUE
           END-PERFORM
              DISPLAY "PROVINCE" PROVINCE-ID.
           STOP RUN .   



                                                                                


             


